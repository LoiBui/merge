(function () {
    'use strict';
    var Skyway = function () {
        var self = this;
    };

    let peer = null;
    let localStream = null;
    let existingCall = null;
    let screenShare = null;
    let isAudioOn = true;
    let isVideoOn = true;
    let rootSelector = '';
    let width = 1280;
    let height = 720;
    let localVideo = null;
    let isShareScreen = false;
    let isNoiseCancellation = true;
    let isAutomaticGainControl = true;
    let isEchoCancellations = true;
    let mediaOptions = {
      audio: {
          echoCancellation: isEchoCancellations,
          noiseSuppression: isNoiseCancellation,
          autoGainControl: isAutomaticGainControl
      },
      video: {
          width: 1280,
          height: 720,
          frameRate: {
              min: 15,
              max: 30
          }
      },
    }

    let context = new (window.AudioContext || window.webkitAudioContext)();
    let micContext = new (window.AudioContext || window.webkitAudioContext)();
    let saved = null;
    let speakerVolumeMeter = null;
    let micVolumeMeter = null;
    let baseUrl = window.location.origin;
    console.log(baseUrl);
    let speakerTestAudioUrl = baseUrl + '/assets/audios/piano.mp3';
    let volumeMeterWidth = 250;
    let meter = null;
    let speakerMeter = null;

    let testCameraViewer = null;

    function onLevelChange() {
        if (meter.checkClipping()) {
            micVolumeMeter.style.background = "red";
        } else {
            micVolumeMeter.style.background = "#1E90FF";
        }
        micVolumeMeter.style.width =  Math.floor(meter.volume * volumeMeterWidth * 1.4) + "px";
        window.requestAnimationFrame(onLevelChange);
    }

    function onSpeakerLevelChange() {
        if (speakerMeter.checkClipping()) {
            speakerVolumeMeter.style.background = "red";
        } else {
            speakerVolumeMeter.style.background = "#1E90FF";
        }
        speakerVolumeMeter.style.width =  Math.floor(speakerMeter.volume * volumeMeterWidth * 1.4) + "px";
        window.requestAnimationFrame(onSpeakerLevelChange);
    }

    Skyway.prototype = {
        playSound: function (arraybuffer, deviceId) {
            let self = this;
            context.close();
            context	= new (window.AudioContext || window.webkitAudioContext)();
            var source = context.createBufferSource();
            source.buffer = arraybuffer;
            speakerMeter = createAudioMeter(context);
            source.connect(speakerMeter);
            source.start();
            onSpeakerLevelChange();

            const speakerTestAudio = new Audio(speakerTestAudioUrl);
            speakerTestAudio.setSinkId(deviceId).then(function () {
                speakerTestAudio.play();
            });
        },
        analyseSpeaker: async function (selector, deviceId) {
            let self = this;
            var divs = document.querySelectorAll('flt-platform-view');

            [].forEach.call(divs, function (div) {
                if (div.shadowRoot.getElementById(selector)) {
                    speakerVolumeMeter = div.shadowRoot.getElementById(selector);
                }
            });

            if (!speakerVolumeMeter) {
                console.log('Speaker volume meter not found')
            } else {
                speakerVolumeMeter.style.background = "#1E90FF";
                if (!saved) {
                    var request = new XMLHttpRequest();
                    request.open('GET', speakerTestAudioUrl, true);
                    request.responseType = 'arraybuffer';
                    request.onload = function () {
                        //take the audio from http request and decode it in an audio buffer
                        context.decodeAudioData(request.response, function (buffer) {
                            // save buffer, to not load again
                            saved = buffer;
                            // play sound
                            self.playSound(buffer, deviceId);
                        });
                    };
                    request.send();
                } else {
                    self.playSound(saved, deviceId);
                }
            }
        },

        recordAndPlayAudio: function() {
            let self = this;
            navigator.getUserMedia({audio: true}, function(stream) {
                const audioRecorder = new MediaRecorder(stream);
                audioRecorder.start();

                const audioChunks = [];
                audioRecorder.addEventListener("dataavailable", event => {
                  audioChunks.push(event.data);
                });

                audioRecorder.addEventListener("stop", () => {
                    const audioBlob = new Blob(audioChunks);
                    const audioUrl = URL.createObjectURL(audioBlob);
                    const audio = new Audio(audioUrl);
                    audio.addEventListener("ended", function(){
                        let event = new CustomEvent('stopRecord', {
                            detail: {
                                action: 'stopRecord',
                            }
                        });
                        document.dispatchEvent(event);
                    });
                    audio.play();
                });

                setTimeout(() => {
                    let event = new CustomEvent('playRecord', {
                        detail: {
                            action: 'playRecord',
                        }
                    });
                    document.dispatchEvent(event);
                    audioRecorder.stop();
                }, 5000);
            }, function() {
                console.log('can not access microphone');
            });
        },

        onMicrophoneGranted: function(stream) {
            micContext.close();
            micContext	= new (window.AudioContext || window.webkitAudioContext)();
            let source = micContext.createMediaStreamSource(stream);
            meter = createAudioMeter(micContext);
            source.connect(meter);
            onLevelChange();
        },

        onMicrophoneDenied: function() {
            console.log('can not access microphone');
        },

        analyseMic: async function (selector, deviceId) {
            let self = this;
            var divs = document.querySelectorAll('flt-platform-view');

            [].forEach.call(divs, function (div) {
                if (div.shadowRoot.getElementById(selector)) {
                    micVolumeMeter = div.shadowRoot.getElementById(selector);
                }
            });

            if (!micVolumeMeter) {
                console.log('Mic volume meter not found')
            } else {
                console.log('analyseMic');
                micVolumeMeter.style.background = "#1E90FF";
                try {
                    let constraints = { "deviceId": { "exact": deviceId } };
                    navigator.getUserMedia(
                        {
                            "audio": constraints,
                        }, self.onMicrophoneGranted, self.onMicrophoneDenied
                    );
                } catch (e) {
                    console.log(e);
                }
            }
        },

        testCamera: function(selector, deviceId, widthVideo, heightVideo, ratioType) {
            var divs = document.querySelectorAll('flt-platform-view');

            [].forEach.call(divs, function (div) {
                if (div.shadowRoot.getElementById(selector)) {
                    testCameraViewer = div.shadowRoot.getElementById(selector);
                }
            });

            if (!testCameraViewer) {
                console.log('test camera viewer not found')
            } else {
                try {
                    let constraints = {
                        "deviceId": { "exact": deviceId },
                        "width": widthVideo,
                        "height": heightVideo,
                    };
                    if(ratioType === 0) {
                        constraints = {
                            "deviceId": { "exact": deviceId },
                            "width": widthVideo,
                            "height": heightVideo,
                            "aspectRatio": { "exact": 1.7777777778 },
                        };
                    }
                    console.log(constraints);
                    navigator.mediaDevices.getUserMedia({video: constraints})
                    .then(function(stream) {
                        testCameraViewer.srcObject = stream;
                        testCameraViewer.playsInline = true;
                        testCameraViewer.play();
                    })
                    .catch(function(e) {
                       console.log(e);
                    });
                } catch (e) {
                    console.log(e);
                }
            }

        },
        async initPeer(key) {
            return new Promise((resolve, reject) => {
                window.peer = peer = new Peer({
                  key: key,
                  debug: 2,
                });

                peer.once("open", () => {
                    peer.removeListener("error", reject);
                    resolve(peer);
                });
                // for onOpen error
                peer.once("error", reject);
            });
        },
        changeDevices: function(videoDeviceId, audioDeviceId, targetPeerId) {
            if (existingCall) {
                existingCall.send({
                    action: 'change devices',
                    videoDeviceId: videoDeviceId,
                    audioDeviceId: audioDeviceId,
                    targetPeerId: targetPeerId,
                })
            }
        },

        connectJoinRoom: async function(key, roomName, selector, rootSelectorElement, widthVideo, heightVideo) {
            let self = this;
            if (existingCall) {
                existingCall.close(true);
            }

            localVideo = document.getElementById(selector);
            await this.initPeer(key);
            if (!roomName || !localVideo) {
                let err = '';
                if (!roomName) {
                    err = new Error('Room name must be defined.');
                } else if (!localVideo) {
                    err = new Error("Video can't found");
                }
                err.type = 'room-error';

                let event = new CustomEvent('roomConn', {
                    detail: {
                        error: err,
                    }
                });
                document.dispatchEvent(event);
                return self;
            }

            width = widthVideo;
            height = heightVideo;

            rootSelector = rootSelectorElement;

            localStream = await navigator.mediaDevices.getUserMedia({
                audio: true,
                video: true,
            });

            localVideo.srcObject = localStream;
            localVideo.playsInline = true;
            await localVideo.play();

            const videoDom = document.createElement('video');
            videoDom.width = 300;
            videoDom.height = 250;
            videoDom.id = 'my-video';
            videoDom.srcObject = localStream;
            var divs = document.querySelectorAll('flt-platform-view');
            let listCamera = null;
            listCamera = document.getElementById(rootSelector);
            if (listCamera) {
                let cameraDivs = listCamera.getElementsByTagName("div")
                console.log('list camera: ', cameraDivs.length);
                let isExisting = false;
                for(let index = 0; index < cameraDivs.length; index++) {
                    if (cameraDivs[index].id == ('video-box-' + peer.id)) {
                        listCamera.removeChild(cameraDivs[index]);
                    }
                }
                const containerElement = document.createElement('div')
                const containerElementPeerId = peer.id;
                containerElement.id = 'video-box-' + peer.id;
                containerElement.setAttribute('part', 'video-box-small');
                containerElement.addEventListener('contextmenu', function(e) {
                    self.sendEvent('otherParticipantRightClick', {
                        x: e.clientX,
                        y: e.clientY,
                        peerId: containerElementPeerId,
                    });
                    e.preventDefault();
                });
                await containerElement.append(videoDom);
                await listCamera.append(containerElement);
                await videoDom.play();
            }

            let stream = await navigator.mediaDevices.getUserMedia({
                audio: true,
                video: {
                    width,
                    height,
                    frameRate: {
                        min: 15,
                        max: 30
                    }
                },
            });

            const call = await peer.joinRoom(roomName, {
                mode: 'sfu',
                stream: stream,
            });

            this.sendEvent('getPeerID', { peerID: peer.id });

            await this.setupCallEventHandlers(call);
            return self;
        },
    setupCallEventHandlers: function (call) {
        let self = this;
        window.existingCall = existingCall = call;
        call.on('stream', function (stream) {
            self.addVideo(stream);
            self.sendEvent('userConnectRoom', {
                peerId: stream.peerId,
                action: 'connect',
            })
        });

        call.on('peerLeave', function (peerId) {
            self.removeVideo(peerId);
            let event = new CustomEvent('userLeaveRoom', {
                detail: {
                    peerId: peerId,
                    action: 'leave',
                }
            });
            document.dispatchEvent(event);
        });

        call.on('peerJoin', function (peerId) {
            existingCall.send({
                action: 'send msg',
                data: localStorage.getItem('history_message'),
            })
        });

        call.on('close', function () {
            localStorage.removeItem('history_message');

            let event = new CustomEvent('userCloseRoom', {
                detail: {
                    action: 'close',
                }
            });
            document.dispatchEvent(event);
            existingCall = null;
        });

        call.on('data', ({ data, src }) => {
            console.log('on data');
            if (data.action == 'remove peerID') {
                if (peer.id === data.peerId) {
                    self.close();
                    let event = new CustomEvent('userRemoveRoom', {
                        detail: {
                            action: 'remove',
                        }
                    });
                    document.dispatchEvent(event);
                }
            } else if (data.action == 'send msg') {
                if (data.uuID) {
                    let message = {
                        uuID: data.uuID,
                        userID: data.userID ? data.userID : '',
                        time: data.time ? data.time : '',
                        peerID: data.peerID ? data.peerID : '',
                        targetPeerID: data.targetPeerID ? data.targetPeerID : '',
                        msg: data.msg ? data.msg : '',
                    };
                    self.sendEvent(
                        'userSentMessage',
                        message,
                    );
                    self.saveHistory(message);
                } else {
                    let historyMessage = data.data;
                    historyMessage = JSON.parse(historyMessage);
                    if (historyMessage !== '' && historyMessage !== null) {
                        self.saveHistoryWithArrayData(historyMessage);

                        self.getMessageWithPeerID('');
                    }
                }
            } else if (data.action == 'send info') {
                self.sendEvent('paticipantInfo', {
                    info: data.info,
                });
            } else if (data.action == 'request info') {
                console.log('receive request info');
                self.sendEvent('onRequestPaticipantInfo', {
                    targetPeerId: data.targetPeerId,
                });
            } else if (data.action == 'change devices') {
                if (peer.id === data.targetPeerId) {
                    self.setDevice(data.videoDeviceId, data.audioDeviceId);
                }
            } else {
                var message = new Message(data);
                if (message.isPayload()) message.emit()
            }
        });
    },
    setDevice: function(videoDeviceId, audioDeviceId) {
        try {
            var divs = document.querySelectorAll('flt-platform-view');
            let myVideo = null;
            [].forEach.call(divs, function (div) {
                if (div.shadowRoot.getElementById('my-video')) {
                    myVideo = div.shadowRoot.getElementById('my-video');
                }
            });
            let videoConstraints = {
                "deviceId": { "exact": videoDeviceId },
            };
            let audioConstraints = {
                "deviceId": { "exact": audioDeviceId },
            };
            navigator.mediaDevices.getUserMedia({video: videoConstraints, audio: audioConstraints})
            .then(async function(stream) {
                myVideo.srcObject.getTracks()[0].stop();
                myVideo.srcObject = stream;
                myVideo.play();
                await existingCall.replaceStream(stream);
                console.log("replaced stream");
            })
            .catch(function(e) {
               console.log(e);
            });
        } catch (e) {
            console.log(e);
        }
    },
    close: function() {
        let self = this;
        if (existingCall) {
            existingCall.close(true);
        }
        self.removeAllRemoteVideos();
    },
    destroy: function() {
        existingCall.destroy();
    },
    addVideo: async function(stream) {
        let self = this;
        const videoDom = document.createElement('video');
        videoDom.width = 300;
        videoDom.height = 250;
        videoDom.id = 'video-' + stream.peerId;
        videoDom.srcObject = stream;
        var divs = document.querySelectorAll('flt-platform-view');
        let listCamera = null;
        [].forEach.call(divs, function (div) {
            if (div.shadowRoot.getElementById(rootSelector)) {
                listCamera = div.shadowRoot.getElementById(rootSelector);
            }
        });

        if (listCamera) {
            const containerElement = document.createElement('div');
            const containerElementPeerId = stream.peerId;
            containerElement.id = 'video-box-' + stream.peerId;
            containerElement.setAttribute('part', 'video-box-small');
            containerElement.addEventListener('contextmenu', function(e) {
                console.log('right click: '+ e.clientX + ', ' + e.clientY);
                self.sendEvent('otherParticipantRightClick', {
                    x: e.clientX,
                    y: e.clientY,
                    peerId: containerElementPeerId,
                });
                e.preventDefault();
            }, false);
            await containerElement.append(videoDom);
            await listCamera.append(containerElement);
            await videoDom.play().catch(console.error);
        }
    },
    removeVideo: async function(peerId) {
      var video = document.getElementById('video-' + peerId);
      if(video){
          video.remove();
      }
    },
    removeAllRemoteVideos: async function() {
        let listCamera = null;
        var divs = document.querySelectorAll('flt-platform-view');
        [].forEach.call(divs, function (div) {
            if (div.shadowRoot.getElementById(rootSelector)) {
                listCamera = div.shadowRoot.getElementById(rootSelector);
            }
        });
        if (listCamera != null) {
            listCamera.innerHTML = '';
        }
    },
    toggleAudio: function(status = undefined) {
        if (status === isAudioOn) return

        isAudioOn = typeof status === 'boolean' ? status : !isAudioOn;
        localStream.getAudioTracks().forEach(track => (track.enabled = isAudioOn));
        existingCall._localStream.getAudioTracks().forEach(track => (track.enabled = isAudioOn));
        var payload = {
            sender: peer.id,
            target: peer.id,
            type: 'toggleAudio',
            value: isAudioOn,
        }
        existingCall.send(Message.payloadToMessage(payload));
    },
    toggleVideo: function(status = undefined) {
        if (status === isVideoOn) return

        isVideoOn = typeof status === 'boolean' ? status : !isVideoOn;
        localStream.getVideoTracks().forEach(track => (track.enabled = isVideoOn));
        existingCall._localStream.getVideoTracks().forEach(track => (track.enabled = isVideoOn));
        var payload = {
            sender: peer.id,
            target: peer.id,
            type: 'toggleVideo',
            value: isVideoOn,
        }
        existingCall.send(Message.payloadToMessage(payload));
    },
    toggleNoiseCancellation: async function(status = undefined) {
      if (status === isNoiseCancellation) return
      if (!existingCall) return;

      isNoiseCancellation = typeof status === 'boolean' ? status : !isNoiseCancellation;
      mediaOptions.audio.noiseSuppression = isNoiseCancellation;
      let newStream = await navigator.mediaDevices.getUserMedia(mediaOptions);
      existingCall.replaceStream(newStream);
    },
    toggleAutomaicGainControl: async function(status = undefined) {
        if (status === isAutomaticGainControl) return
        if (!existingCall) return;

        isAutomaticGainControl = typeof status === 'boolean' ? status : !isAutomaticGainControl;
        mediaOptions.audio.autoGainControl = isAutomaticGainControl;
        let newStream = await navigator.mediaDevices.getUserMedia(mediaOptions);
        existingCall.replaceStream(newStream);
    },
    toggleEchoCancellation: async function(status = undefined) {
        if (status === isEchoCancellations) return
        if (!existingCall) return;

        isEchoCancellations = typeof status === 'boolean' ? status : !isEchoCancellations;
        mediaOptions.audio.echoCancellation = isEchoCancellations;
        let newStream = await navigator.mediaDevices.getUserMedia(mediaOptions);
        existingCall.replaceStream(newStream);
    },
    requestToggleAudio: function(peerId, status) {
        var payload = {
            sender: peer.id,
            target: peerId,
            type: 'requestToggleAudio',
            value: status,
        }
        existingCall.send(Message.payloadToMessage(payload));
    },
    requestToggleVideo: function(peerId, status) {
        var payload = {
            sender: peer.id,
            target: peerId,
            type: 'requestToggleVideo',
            value: status,
        }
        existingCall.send(Message.payloadToMessage(payload));
    },
    requestToggleAllOtherAudio: function(status, options = {}) {
        var payload = {
            sender: peer.id,
            type: 'requestToggleAllOtherAudio',
            value: status,
            excludes: [options.excludes],
        }
        existingCall.send(Message.payloadToMessage(payload));
    },
    startShareScreen: async function() {
        let self = this;
        if (existingCall !== null) {
            const stream = await navigator.mediaDevices.getDisplayMedia({
                audio: true,
                video: true,
                width,
                height,
                frameRate: 30
            });
            var divs = document.querySelectorAll('flt-platform-view');
            let myVideo = null;
            [].forEach.call(divs, function (div) {
                if (div.shadowRoot.getElementById('my-video')) {
                    myVideo = div.shadowRoot.getElementById('my-video');
                }
            });
            myVideo.srcObject = stream;
            await existingCall.replaceStream(stream);
            stream.getVideoTracks()[0].onended = function () {
                self.sendEvent('shareScreenStopped', null);
                self.stopShareScreen();
            };
        }
        isShareScreen = true;
    },
    stopShareScreen: async function() {
        if (existingCall) {
            var divs = document.querySelectorAll('flt-platform-view');
            let myVideo = null;
            [].forEach.call(divs, function (div) {
                if (div.shadowRoot.getElementById('my-video')) {
                    myVideo = div.shadowRoot.getElementById('my-video');
                }
            });
            myVideo.srcObject.getTracks()[0].stop();
            myVideo.srcObject = localStream;
            myVideo.play().catch(e => console.log(e));

            await existingCall.replaceStream(localStream);
            isShareScreen = false;
        }
    },
    changeTheVideoResolution: async function(widthElement, heightElement, videoBandwidth) {
        width = widthElement;
        height = heightElement;
        let changeStream = null;
        if (!isShareScreen) {
            changeStream = await navigator.mediaDevices.getUserMedia({
                audio: true,
                video: {
                    width: width,
                    height: height,
                    frameRate: {
                        min: 15,
                        max: 30
                    }
                },
            }).catch(console.error);
            await existingCall.replaceStream(changeStream);
        } else {
            screenShare.start({
                width,
                height,
                frameRate: 30
            }).then(async stream => {
                await existingCall.replaceStream(changeStream);

            })
        }
    },
    deleteTheUserFromTheRoom(peerId) {
        existingCall.send({
            action: 'remove peerID',
            peerId
        })
    },
    sendEmoticon(name, mode, path) {
        var emoticon = new Emoticon(window.peer.id, name, mode, path);
        emoticon.show();
    },
    removeDuplicateMessage(historyMessage) {
        let historyMessageTmp = historyMessage.filter((v, i, a) => a.findIndex(t => (t.uuID === v.uuID)) === i)
        return historyMessageTmp;
    },
    sendMessage(uuID, time, userID, peerID, targetPeerID, msg) {
        if (existingCall) {
            this.saveHistory({
                uuID,
                userID,
                time,
                peerID,
                targetPeerID,
                msg
            });

            existingCall.send({
                action: 'send msg',
                uuID: uuID,
                time,
                userID: userID,
                peerID,
                targetPeerID,
                msg,
            })
        }
    },
    sendParticipantInfo(info) {
        if (existingCall) {
            existingCall.send({
                action: 'send info',
                info: info,
            })
        }
    },
    requestParticipantInfo(targetPeerId) {
        if (existingCall) {
            console.log('send participant info request');
            existingCall.send({
                action: 'request info',
                targetPeerId: targetPeerId,
            })
        }
    },
    sendEvent(title, params) {
        let event = new CustomEvent(title, {
            detail: params
        });

        document.dispatchEvent(event);
    },
    saveHistory(message) {
        let historyMessage = localStorage.getItem('history_message');
        if (!historyMessage) {
            localStorage.setItem('history_message', JSON.stringify([message]));
        } else {
            let newMessage = [];
            historyMessage = JSON.parse(historyMessage);
            historyMessage.forEach(e => {
                if (e.uuID != message.uuID && (message.targetPeerID == '' || message.targetPeerID == peer.id || message.peerID == e.peerID)) {
                    newMessage.push(message);
                }
            });
            historyMessage = historyMessage.concat(newMessage);
            historyMessage.sort(function (a, b) {
                return parseInt(a.time) - parseInt(b.time);
            });
            historyMessage = this.removeDuplicateMessage(historyMessage);
            localStorage.setItem('history_message', JSON.stringify(historyMessage));
        }
    },
    saveHistoryWithArrayData(messages) { // message is array
        let historyMessage = localStorage.getItem('history_message');
        if (!historyMessage) {
            localStorage.setItem('history_message', JSON.stringify(messages));
        } else {
            historyMessage = JSON.parse(historyMessage);
            let newMessage = messages
                .filter(message => !historyMessage.some(item => (
                    message.userID === item.userID
                )));
            historyMessage = historyMessage.concat(newMessage);
            historyMessage.sort(function (a, b) {
                return parseInt(a.time) - parseInt(b.time);
            });
            historyMessage = this.removeDuplicateMessage(historyMessage);
            localStorage.setItem('history_message', JSON.stringify(historyMessage));
        }
    },
    async getMessageWithPeerID(targetPeerID) {
        let self = this;
        let historyMessage = localStorage.getItem('history_message');
        historyMessage = JSON.parse(historyMessage);
        let listMessage = [];
        if (historyMessage) {
            historyMessage.forEach(element => {
                if (targetPeerID != '') {
                    if (element.targetPeerID == peer.id) {
                        listMessage.push(element);
                    }
                    if (element.peerID == peer.id) {
                        listMessage.push(element);
                    }
                } else {
                    if (!element.targetPeerID) {
                        listMessage.push(element);
                    }
                }
            });
        }

        await self.sendEvent(
            'historyMessage',
            JSON.stringify(listMessage),
        );
    },
}

  if (typeof exports !== 'undefined') {
    exports.Skyway = Skyway;
}

if (typeof window !== 'undefined') {
    window.Skyway = Skyway;
} else if (typeof global !== 'undefined') {
    global.Skyway = Skyway;
}
}) ();